﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.InteropServices;
using System.Threading;
using System.Diagnostics;

namespace PingWrapper
{
    class PingHolder
    {
        [DllImport("ntdll.dll", SetLastError = true)] public static extern IntPtr NtSuspendProcess(IntPtr ProcessHandle);
        [DllImport("ntdll.dll", SetLastError = true)] public static extern IntPtr NtResumeProcess(IntPtr ProcessHandle);

        Form2 form;
        public delegate void MessageCallback(string strText);
        public MessageCallback messageCallback;
        System.Diagnostics.Process terminal;
        bool paused;

        public PingHolder(string param, MessageCallback del, Form2 form)
        {
            paused = false;
            this.form = form;
            messageCallback = del;
            terminal = new System.Diagnostics.Process();
            Init(GenInfo(param));
        }
        private ProcessStartInfo GenInfo(string param)
        {
            ProcessStartInfo info = new ProcessStartInfo();
            info.FileName = "ping.exe";
            info.Arguments = param;
            info.UseShellExecute = false;
            info.CreateNoWindow = true;
            info.RedirectStandardOutput = true;
            info.StandardOutputEncoding = Encoding.GetEncoding("cp866");
            return info;
        }
        private void Init(ProcessStartInfo info)
        {
            terminal.StartInfo = info;
            terminal.EnableRaisingEvents = true;
            terminal.OutputDataReceived += new System.Diagnostics.DataReceivedEventHandler(ConsoleOutputHandler);
            terminal.Start();
            terminal.BeginOutputReadLine();
        }
        public void PlayPause()
        {
            if (paused)
                NtResumeProcess(terminal.Handle);
            else
                NtSuspendProcess(terminal.Handle);
            paused = !paused;
        }
        private void ConsoleOutputHandler(object sendingProcess, System.Diagnostics.DataReceivedEventArgs outLine)
        {
            if (!String.IsNullOrEmpty(outLine.Data))
            {
                if (form.InvokeRequired)
                    form.Invoke(messageCallback, outLine.Data);
                else
                    messageCallback(outLine.Data);
            }
        }
        public void Stats()
        {
            Process s = new Process();
            s = new System.Diagnostics.Process();
            s.StartInfo.FileName = "SendSignal.exe";
            s.StartInfo.Arguments = Convert.ToString(terminal.Id);
            s.StartInfo.UseShellExecute = false;
            s.StartInfo.CreateNoWindow = true;
            s.Start();
        }
        public void Exit()
        {
            try
            {
                if (!terminal.HasExited) terminal.Kill();
                     terminal.Close();
            }
            catch (Exception e) { }
        }
    }
}
