﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using Newtonsoft.Json;
using System.Diagnostics;

namespace PingWrapper
{
    public class KeyConfig
    {
        public string top { get; set; }
        public string mode { get; set; }
        public string pause { get; set; }
        public string clean { get; set; }
        public string stats { get; set; }
        public string save { get; set; }
        public string time { get; set; }
        public string copy { get; set; }
    }

    public class Config
    {
        public string default_param { get; set; }
        public string size_multiplier { get; set; }
        public string text_size { get; set; }
        public string standart_ping { get; set; }
        public string background_color1 { get; set; }
        public string background_color2 { get; set; }
        public string default_color { get; set; }
        public KeyConfig key_config { get; set; }
        public List<string> ignore { get; set; }
        public List<List<string>> colors { get; set; }
    }

        public static class Settings
        {
            public static Config config;
            public static void Load()
            {
                using (StreamReader r = new StreamReader("config.json"))
                {
                    string json = r.ReadToEnd();
                    System.Diagnostics.Debug.WriteLine(json);
                    config = JsonConvert.DeserializeObject<Config>(json);
                }
            }
        }
}