﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PingWrapper
{
    public partial class Form3 : Form
    {
        public Form3()
        {
            InitializeComponent();
            Text = Globals.param;
        }

        private void Form3_Activated(object sender, EventArgs e)
        {
            if(Program.form2.Visible)
            {
                Program.form2.Show();
                Program.form2.Focus();
            }
            Program.form1.Show();
            Program.form1.Focus();
        }

        private void Form3_FormClosing(object sender, FormClosingEventArgs e)
        {
            Program.form2.pingHolder.Exit();
            Application.Exit();
        }

        private void Form3_Load(object sender, EventArgs e)
        {
            ModeController.SetCurrentMode();
            if(Globals.full)
            {
                Program.form2.Top = Program.form1.Top;
                Program.form2.Left = Program.form1.Left+Program.form1.Width*Convert.ToInt32(Settings.config.size_multiplier);
            }
        }
    }
}
