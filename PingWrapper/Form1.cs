﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Threading;
using System.Runtime.InteropServices;
using System.Text.RegularExpressions;


namespace PingWrapper
{
    partial class Form1 : Form
    {
        private int iFormX, iFormY, iMouseX, iMouseY; //Отвечают за перетаскивание формы
        public Form1(string param)
        {
            InitializeComponent();
            if (Regex.IsMatch(param, " -full"))
            {
                Globals.full = true;
                param = Regex.Replace(param, " -full", " ");
            }
            if (Regex.IsMatch(param, " -fix"))
            {
                Globals.top = true;
                param = Regex.Replace(param, " -fix", " ");
            }
            if (Regex.IsMatch(param, " -mul [0-9]*"))
            {
                Settings.config.size_multiplier = Regex.Match(param, "(?<=-mul )([0-9]*)").Value;
                param = Regex.Replace(param, " -mul [0-9]*", " ");
            }
            if (Regex.IsMatch(param, " -stdp [0-9]*"))
            {
                Settings.config.standart_ping = Regex.Match(param, "(?<=-stdp* )([0-9]*)").Value;
                param = Regex.Replace(param, " -stdp [0-9]*", " ");
            }
            if (Regex.IsMatch(param, " -st"))
            {
                Globals.stampEnabled = true;
                param = Regex.Replace(param, " -st", " ");
            }
            Globals.param = param;
            this.Text = param;
            BackColor = System.Drawing.ColorTranslator.FromHtml(Settings.config.background_color1);
            HideFromAltTab(Handle);
        }

        [DllImport("user32.dll")]
        private static extern int SetWindowLong(IntPtr window, int index, int value);

        [DllImport("user32.dll")]
        private static extern int GetWindowLong(IntPtr window, int index);

        private const int GWL_EXSTYLE = -20;
        private const int WS_EX_TOOLWINDOW = 0x00000080;

        public static void HideFromAltTab(IntPtr Handle)
        {
            SetWindowLong(Handle, GWL_EXSTYLE, GetWindowLong(Handle,
                GWL_EXSTYLE) | WS_EX_TOOLWINDOW);
        }
        
        private void Form1_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == System.Windows.Forms.MouseButtons.Middle) Application.Exit();
            iFormX = this.Location.X;
            iFormY = this.Location.Y;
            iMouseX = MousePosition.X;
            iMouseY = MousePosition.Y;
        }

        private void Form1_MouseMove(object sender, MouseEventArgs e)
        {
            int iMouseX2 = MousePosition.X;
            int iMouseY2 = MousePosition.Y;
            if (e.Button == MouseButtons.Left)
            {
                bool connectedl = false, connectedr = false;
                if (Program.form2.Location.X == Location.X - Program.form2.Width) connectedl = true;
                if (Program.form2.Location.X == Location.X + Width) connectedr = true;
                this.Location = new Point(iFormX + (iMouseX2 - iMouseX), iFormY + (iMouseY2 - iMouseY));
                if (connectedr)
                    ModeController.SetRight();
                if (connectedl)
                    ModeController.SetLeft();
            }
                
        }

        private void Form1_KeyPress(object sender, KeyPressEventArgs e)
        {
            string k = Convert.ToString((int)e.KeyChar);
            if (Settings.config.key_config.mode == k) ModeController.ChangeMode();
            if (Settings.config.key_config.clean == k) Program.form2.Clear();
            if (Settings.config.key_config.copy == k) Program.form2.Copy();
            if (Settings.config.key_config.pause == k) Program.form2.Pause();
            if (Settings.config.key_config.save == k) Program.form2.Save();
            if (Settings.config.key_config.stats == k) Program.form2.pingHolder.Stats();
            if (Settings.config.key_config.time == k) Globals.stampEnabled = !Globals.stampEnabled;
            if (Settings.config.key_config.top == k) Program.form2.SetTop();
            System.Diagnostics.Debug.WriteLine(Convert.ToString((int)e.KeyChar));
        }

        private void Form1_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            ModeController.ChangeMode();
        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            Program.form2.pingHolder.Exit();
            Application.Exit(e);
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            Scale(new SizeF(Convert.ToInt32(Settings.config.size_multiplier), Convert.ToInt32(Settings.config.size_multiplier)));
        }

        private void Form1_Enter(object sender, EventArgs e)
        {
            
        }

        private void Form1_Activated(object sender, EventArgs e)
        {
            
        }

        private void Form1_MouseUp(object sender, MouseEventArgs e)
        {
            
        }

        private void Form1_Leave(object sender, EventArgs e)
        {
 
        }

        private void Form1_Deactivate(object sender, EventArgs e)
        {
            
        }

    }
}
