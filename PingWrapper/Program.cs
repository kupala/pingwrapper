﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PingWrapper
{
    static class Program
    {
        internal static Form1 form1;
        internal static Form2 form2;
        internal static Form3 form3;
        [STAThread]
        static void Main(string[] args)
        {
            string param = "";
            if(args.Length == 0)
            {
                MessageBox.Show(" -full показ текстовой формы\n -fix поверх всех окон\n -st включить штамп\n -mul [число] множитель\n -stdp [число] пинг");
                System.Environment.Exit(0);
            }
                
            for (int i = 0; i < args.Length; i++ )
            {
                param += args[i]+" ";
            }
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Settings.Load();
            form1 = new Form1(param + Settings.config.default_param);
            form2 = new Form2();
            form3 = new Form3();
            Application.Run(form3);
        }
    }
}
