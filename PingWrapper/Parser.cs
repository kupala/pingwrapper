﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using System.Text.RegularExpressions;

namespace PingWrapper
{
    class Parser
    {
        string message;
        public Parser(string message)
        {
            this.message = message;
        }
        public string Text()
        {
            string msg = "";
            if (!Program.form2.IsEmpty())
                msg += System.Environment.NewLine;
            if (Globals.stampEnabled)
                msg += StampGen();
            msg += message;
            return msg;
        }
        public Color Color()
        {
            foreach(var pair in Settings.config.colors)
            {
                Regex r = new Regex(pair[0]);
                if (r.IsMatch(message)) return ColorTranslator.FromHtml(pair[1]);
            }
            return ColorTranslator.FromHtml(Settings.config.default_color);
        }
        public int Length()
        {
            Regex r = new Regex("(?<=время=)(.*)(?=мс)");
            if (!r.IsMatch(message)) return 50;
            int n = Convert.ToInt32(r.Match(message).Value);
            int ping = Convert.ToInt32(Settings.config.standart_ping);
            float perc = (float)(ping - n) / ping;
            n = Math.Max(2,(int)(perc*50));
            return n;
        }
        public bool Ignore()
        {
            foreach (var pair in Settings.config.ignore)
            {
                Regex r = new Regex(pair);
                if (r.IsMatch(message)) return true;
            }
            return false;
        }
        public static void ChangeStampMode()
        {
            Globals.stampEnabled = !Globals.stampEnabled;
        }
        private string StampGen()
        {
            return String.Format("[{0:H:mm:ss}] ", DateTime.Now);
        }
    }
}
